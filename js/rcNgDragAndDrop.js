/*
 * rcNgDragAndDrop - Simple AngularJS directive to drag & drop element using HammerJS
 * http://github.com/jean-rakotozafy/rcNgMovable
 * (c) 2014 MIT License, https://www.m-ite.com
 */

(function (window, angular) {
    'use strict';

    var module = angular.module('rcNgDragAndDrop', []);

    module.constant('DragDropHammer', Hammer);

    module.constant('hammerConfigDragDrop', {
        transformMinScale: 1,
        transformMinRotation: 0,
        dragBlockHorizontal: true,
        dragBlockVertical: true,
        dragMinDistance: 0
    });

    module.value('droppables', []);

    module.factory('rcNgDragAndDropService', ['droppables', '$q', function (droppables, $q) {

        var Droppable = function ($el) {
            this.d = $q.defer();
            this.$el = $el;
            this.isBeingDraggedOver = false;
            this.promise = this.d.promise;
        };

        Droppable.prototype = {
            contains: function (point) {
                var rect = this.$el.offset();
                var w = this.$el.width();
                var h = this.$el.height();
                //$log.log('point( %f , %f ) - rect( %f , %f , %f , %f )', point.x, point.y, rect.left, rect.top, rect.left + w, rect.top + h);
                return rect.left < point.x && rect.top < point.y && point.x < rect.left + w && point.y < rect.top + h;
            },
            onDragEnter: function (data) {
                this.isBeingDraggedOver = true;
                this.d.notify({type: 'enter', data: data});
            },
            onDragDrop: function (data) {
                this.d.notify({type: 'drop', data: data});
            },
            onDragExit: function (data) {
                this.isBeingDraggedOver = false;
                this.d.notify({type: 'exit', data: data});
            }
        };

        return {
            registerDroppable: function ($el) {
                var droppable = new Droppable($el);
                droppables.push(droppable);
                return droppable.promise;
            },
            removeRegisteredDroppable: function ($el) {
                //TODO or TO REMOVE
            },
            registerDragMouvement: function (data) {
                for (var i = 0; i < droppables.length; i++) {
                    if (droppables[i].contains(data) && !droppables[i].isBeingDraggedOver) {
                        droppables[i].onDragEnter(data);
                    } else if (!droppables[i].contains(data) && droppables[i].isBeingDraggedOver) {
                        droppables[i].onDragExit(data);
                    }
                }
            },
            registerDrop: function (data) {
                var d = $q.defer();
                var droppable;
                var droppedInDroppable = false;
                for (var j = 0; j < droppables.length; j++) {
                    if (droppables[j].isBeingDraggedOver) {
                        droppable = droppables[j];
                        droppable.onDragDrop(data);
                        droppedInDroppable = true;
                    }
                }
                if (droppedInDroppable && droppable)
                    d.resolve({droppable: droppable.$el, dropped: data.$el});
                else
                    d.reject(data.$el);
                return d.promise;
            }
        };
    }]);

})(window, window.angular);
